﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using aggregator;

namespace FarmPlacesParser
{
    public class FarmPlacesParser: IModule, IParserModule
    {
       public string Name { get { return "Аптеки в Москве"; } }

       public string SourceName { get { return "data123.csv"; } }
        public DataModel[] Parse(string data) 
        {
            string city, street;
            string[] farms = data.Split(new string[]{Environment.NewLine},StringSplitOptions.RemoveEmptyEntries);
            List<DataModel> models = new List<DataModel>();
            foreach(var farm in farms.Skip(1))
            {
                city = "Москва";
                street = "Неизвестно";
                if ( !string.IsNullOrEmpty( farm ) )
                {
                    DataModel model = models.FirstOrDefault( m => m.City == city && m.Street == street );
                    if ( model == null )
                    {
                        model = new DataModel() { City = city, Street = street };
                        models.Add( model );
                    }

                    ObjectModel obj = new ObjectModel();
                    Regex csvParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                    string[] parts = csvParser.Split(farm);
                    obj.Kind = parts[8].Trim( '"' );
                    obj.Name = parts[1].Trim( '"' );
                    obj.Properties.Add(new DataProperty("Телефон", parts[6].Trim( '"' ) ) );
                    model.Objects.Add( obj );
                }
            }
            return models.ToArray();
        }
        /*Farms[] farms = 
            foreach(Farms farm in farms)
            {
                string[] lines = File.ReadAllLines( data-20140122T0913-structure-20140122T0913.csv, Encoding.UTF8 );
                int count = 0; // Счетчик строк
                // 
                // Теперь собственно обработка:
                // 
                foreach ( string line in lines )
                    // Защита: чтобы не обрабатывать пустые строки
                    if ( !string.IsNullOrEmpty( line ) )
                    {
                        //string[] parts = line.Split(',');

                        // Теперь по поводу разбиения на строки:
                        // Ты всё правильно написал функцию Split, но мы не учли одной детали:
                        // на сайте data.gov.ru так же, как и в других местах, в csv файлах элементы могут быть в кавычках,
                        // например, если в элемент надо вставить разделить частей, то есть, запятую, например:
                        // 
                        // ,,"51,08",,,1,"АЗС №86030","ООО ""ЛУКОЙЛ-Уралнефтепродукт""","город Когалым, ул. Дружбы Народов, 62","62,254637","74,533541","44,58",,"48,16",,,
                        // 
                        // Следовательно, необходимо использовать регулярные выражения.
                        // Знаешь, что это такое?
                        // Если нет, то можешь поизучать отдельно, штука довольно хитрая, но в программировании незаменимая, особенно, если работаешь с текстами.
                        // Пока же, я нашел на форуме подходящее для csv выражение.
                        // Оно делает то же, что Split, но с учетом кавычек.
                        // 
                        Regex csvParser = new Regex( ",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))" );
                        string[] parts = csvParser.Split( line );
                        // Вызываем парсер
                       
                    }
            }*/ 
    }
}
