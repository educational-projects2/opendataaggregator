﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmPlacesParser
{
    class Farms
    {
        public string Name { get; set; }
        public string Specifical { get; set; }
        public string Address { get; set; }
        public string Number { get; set; }
    }
}
