﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using aggregator;

namespace BarPlacesParser
{
    public class BarPlacesParser : IModule, IParserModule

    {
        public string Name { get { return "Бары в Москве"; } }

        public string SourceName { get { return "data-20150625T0100 (2).json"; } }
        public DataModel[] Parse( string data )
        {
            Bars[] bars = JsonConvert.DeserializeObject<Bars[]>(data);
            List<DataModel> models = new List<DataModel>();
            foreach (Bars item in bars)
            {
                DataModel model = new DataModel();
                // Выделяем из адреса город и улицу
                string[] parts = item.Address.Split(',');
                if (parts.Length > 0)
                    model.City = parts[0].Replace("город", "").Trim();
                if (parts.Length > 1)
                    model.Street = parts[1].Trim();
                model.Address = string.Join(", ", parts.Skip(2));
                if (!string.IsNullOrEmpty(item.Name))
                    model.Properties.Add(new DataProperty("Название", item.Name));
                if (item.PublicPhone.Length > 0)
                    model.Properties.Add(new DataProperty("Телефон", string.Join(", ", item.PublicPhone.Select(pp => pp.PublicPhone))));
                models.Add(model);
            }
            return models.ToArray();
        }
    }
}
