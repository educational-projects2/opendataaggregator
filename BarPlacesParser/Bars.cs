﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarPlacesParser
{
   public class Bars
    {
       public int ID { get; set; }
       public string Name { get; set; }
       public string Address { get; set; }
       public PublicPhone[] PublicPhone { get; set; }
       
    }
}
