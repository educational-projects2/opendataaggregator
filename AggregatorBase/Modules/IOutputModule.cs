﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aggregator
{
    /// <summary>
    /// Интерфейс модуля вывода
    /// </summary>
    public interface IOutputModule
    {
        /// <summary>
        /// Выходной каталог для сохранения данных
        /// </summary>
        string OutputFolder { get; set; }

        /// <summary>
        /// Способ сохранения выходных данных
        /// </summary>
        DataOutputWay OutputWay { get; }

        /// <summary>
        /// Сохранение обработанных данных
        /// </summary>
        /// <param name="dataModel">Данные для сохранения</param>
        void WriteData( DataModel[] dataModel );
    }
}
