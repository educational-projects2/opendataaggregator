﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aggregator
{
    /// <summary>
    /// Интерфейс модуля
    /// </summary>
    public interface IModule
    {
        /// <summary>
        /// Название модуля
        /// </summary>
        string Name { get; }
    }
}
