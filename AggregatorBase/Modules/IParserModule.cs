﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aggregator
{
    /// <summary>
    /// Интерфейс модуля обработки (парсера)
    /// </summary>
    public interface IParserModule
    {
        /// <summary>
        /// Имя файла данных, которые обрабатывает модуль.
        /// </summary>
        string SourceName { get; }

        /// <summary>
        /// Обработка данных
        /// </summary>
        /// <param name="data">Данные в текстовом виде</param>
        /// <returns>Возвращает обработанные данные в виде объекта</returns>
        DataModel[] Parse( string data );
    }
}
