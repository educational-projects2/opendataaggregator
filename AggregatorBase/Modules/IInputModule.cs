﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aggregator
{
    /// <summary>
    /// Интерфейс модуля ввода
    /// </summary>
    public interface IInputModule
    {
        /// <summary>
        /// Источник входных данных
        /// </summary>
        DataInputSource InputSource { get; }

        /// <summary>
        /// Ввод исходных данных
        /// </summary>
        /// <param name="resourceName">Имя ресурса</param>
        /// <returns>Возвращает данные в текстовом виде</returns>
        string ReadData( string resourceName );
    }
}
