﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aggregator
{
    /// <summary>
    /// Источник входных данных
    /// </summary>
    public enum DataInputSource
    {
        /// <summary>
        /// Файл
        /// </summary>
        File,

        /// <summary>
        /// Веб-сайт
        /// </summary>
        Web
    }
}
