﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aggregator
{
    /// <summary>
    /// Способ сохранения выходных данных
    /// </summary>
    public enum DataOutputWay
    {
        /// <summary>
        /// Файл
        /// </summary>
        File,

        /// <summary>
        /// База данных
        /// </summary>
        Database
    }
}
