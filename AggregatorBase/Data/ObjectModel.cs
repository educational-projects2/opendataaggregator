﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aggregator
{
    /// <summary>
    /// Объект
    /// </summary>
    public class ObjectModel
    {
        /// <summary>
        /// Навзание объекта
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Тип(коворкинг,бар и т.д.)
        /// </summary>
        public string Kind { get; set; }

        /// <summary>
        /// Адрес относительно улицы
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Свойства объекта в виде списка свойство=значение
        /// </summary>
        public List<DataProperty> Properties { get; set; }

        public ObjectModel()
        {
            Kind = "Неизвестен";
            Properties = new List<DataProperty>();
        }
    }
}
