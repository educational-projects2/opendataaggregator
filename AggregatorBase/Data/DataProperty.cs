﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aggregator
{
    public class DataProperty
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public DataProperty( string name, string value )
        {
            Name = name;
            Value = value;
        }
    }
}
