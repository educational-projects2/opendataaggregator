﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aggregator
{
    /// <summary>
    /// Модель выходных данных
    /// </summary>
    public class DataModel
    {
        /// <summary>
        /// Страна
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Улица (проспект и т.д.)
        /// </summary>
        public string Street { get; set; }

        public List<ObjectModel> Objects { get; set; } 

        public DataModel()
        {
            Country = "Россия";
            City = "Неизвестно";
            Objects = new List<ObjectModel>();
        }
    }
}
