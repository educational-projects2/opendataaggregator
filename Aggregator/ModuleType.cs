﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aggregator
{
    /// <summary>
    /// Типы модулей
    /// </summary>
    public enum ModuleType
    {
        /// <summary>
        /// Модуль ввода
        /// </summary>
        Input,

        /// <summary>
        /// Модуль вывода
        /// </summary>
        Output,

        /// <summary>
        /// Парсер
        /// </summary>
        Parser
    }
}
