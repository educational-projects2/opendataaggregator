﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aggregator
{
    /// <summary>
    /// Оболочка модулей.
    /// В ней хранятся данные для управления модулем
    /// </summary>
    public class ModuleShell
    {
        /// <summary>
        /// Тип модуля
        /// </summary>
        public ModuleType ModuleType;

        /// <summary>
        /// Экземпляр класса модуля
        /// </summary>
        public IModule Module;

        /// <summary>
        /// Экземпляр класса модуля ввода
        /// </summary>
        public IInputModule InputModule{get{return Module as IInputModule;}}

        /// <summary>
        /// Экземпляр класса модуля вывода
        /// </summary>
        public IOutputModule OutputModule{get{return Module as IOutputModule;}}

        /// <summary>
        /// Экземпляр класса модуля обработки
        /// </summary>
        public IParserModule ParserModule { get { return Module as IParserModule; } } 

        public override string ToString()
        {
            if (Module!=null)
            {
                return Module.Name;
            }
            else
            {
                return base.ToString();
            }
            /// return Module?.Name ?? base.ToString();
        }
    }
}
