﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Diagnostics;

namespace aggregator
{
    public class Aggregator
    {
        /// <summary>
        /// Каталог выходных данных
        /// </summary>
        string _outputFolder = "C:\\Data";

        /// <summary>
        /// Список доступных модулей
        /// </summary>
        List<ModuleShell> _modules = new List<ModuleShell>();

        public Aggregator()
        {
            Init();
        }

        /// <summary>
        /// Инициализация ядра
        /// </summary>
        /// <param name="outputFolder">Выходной каталог</param>
        public Aggregator( string outputFolder )
        {
            _outputFolder = outputFolder;
            Init();
        }

        /// <summary>
        /// Инициализация Агрегатора
        /// </summary>
        private void Init()
        {
            // ЗАГРУЗКА МОДУЛЕЙ
            // Получаем список файлов модулей из папки Modules
            string[] files = Directory.GetFiles( Path.Combine( Path.GetDirectoryName( Process.GetCurrentProcess().MainModule.FileName ), "Modules\\" ), "*.dll" );
            // Создаем список доступных модулей
            foreach ( string filename in files )
            {
                // Защита, чтобы не подключить базовый класс модуля
                if ( Path.GetFileName( filename ).ToLower() == "aggregatorbase.dll" ) continue;
                Assembly assembly = Assembly.LoadFrom( filename );
                Type[] types = assembly.GetTypes();
                foreach ( Type t in types )
                {
                    if ( typeof( IModule ).IsAssignableFrom( t ) )
                    {
                        // Найден модуль - класс, наследуемый от IModule - базового интерфейса модулей
                        // Создаем оболочку модуля
                        ModuleShell shell = new ModuleShell()
                        {
                            Module = Activator.CreateInstance( t ) as IModule
                        };
                        if ( shell.Module is IInputModule )
                            shell.ModuleType = ModuleType.Input;
                        else
                            if ( shell.Module is IOutputModule )
                            shell.ModuleType = ModuleType.Output;
                        else
                            if ( shell.Module is IParserModule )
                            shell.ModuleType = ModuleType.Parser;
                        else
                            // В модуле не определен ни один из интерфейсов - проускаем его
                            continue;
                        _modules.Add( shell );
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Обработка данных из файла
        /// </summary>
        /// <param name="filename"></param>
        public void ProcessFile( string filename )
        {
            ProcessData( filename, DataInputSource.File );
        }

        /// <summary>
        /// Обработка данных из интернета
        /// </summary>
        /// <param name="address"></param>
        public void ProcessWeb( string address )
        {
            ProcessData( address, DataInputSource.Web );
        }

        /// <summary>
        /// Главная функция обработки данных
        /// </summary>
        /// <param name="resourceName">Путь к ресурсу (файлу или веб-сайту)</param>
        /// <param name="source">Тип источника данных</param>
        private void ProcessData( string resourceName, DataInputSource source )
        {
            // Ввод данных
            var inputData = Input( resourceName, source );

            // Обработка данных
            var outputData = Parse( resourceName, inputData );

            // Вывод данных
            Output( outputData );
        }

       /// <summary>
        /// Функция ввода данных
        /// </summary>
        /// <param name="resourceName">Путь к ресурсу (файлу или веб-сайту)</param>
        /// <param name="source">Тип источника данных</param>
        /// <returns>Возвращает данные в текстовом виде</returns>
        private string Input( string resourceName, DataInputSource source )
        {
            // Ищем модуль ввода данных, в котором источник данных совпадает с заданным - source
            var mod = _modules.FirstOrDefault( m => m.ModuleType == ModuleType.Input && m.InputModule.InputSource == source );
            if ( mod == null )
                throw new Exception( "Не найдено подходящего модуля ввода данных." );
            IInputModule inputModule = mod.InputModule;
            return inputModule.ReadData( resourceName );
        }

        /// <summary>
        /// Функция обработки данных
        /// </summary>
        /// <param name="data">Данные для обработки в текстовом виде</param>
        /// <returns>Возвращает данные в виде объекта</returns>
        private DataModel[] Parse( string resourceName, string data )
        {
            // Ищем подходящий модуль обработки данных по имени файла ресурса
            string sourceName = Path.GetFileName( resourceName ).ToLower();
            var mod = _modules.FirstOrDefault( m => m.ModuleType == ModuleType.Parser && m.ParserModule.SourceName.ToLower() == sourceName );
            if ( mod == null )
                throw new Exception( "Не найдено подходящего модуля обработки данных." );
            IParserModule parser = mod.ParserModule;
            return parser.Parse( data );
        }

        /// <summary>
        /// Функция вывода данных
        /// </summary>
        /// <param name="source">Тип источника данных</param>
        /// <returns>Возвращает данные в текстовом виде</returns>
        private void Output( DataModel[] data )
        {
            // Ищем модуль вывода данных
            var mod = _modules.FirstOrDefault( m => m.ModuleType == ModuleType.Output && m.OutputModule.OutputWay == DataOutputWay.File );
            if ( mod == null )
                throw new Exception( "Не найдено подходящего модуля вывода данных." );
            IOutputModule outputModule = mod.OutputModule;
            outputModule.OutputFolder = _outputFolder;
            outputModule.WriteData( data );
        }
    }
}
