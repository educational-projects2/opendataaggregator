﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperMarktNProdParser
{
    public class SupMarkt
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string TypeObject { get; set; }
        public PublicPhone[] PublicPhone { get; set; }
    }
}
