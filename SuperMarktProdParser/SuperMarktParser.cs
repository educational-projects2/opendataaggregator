﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aggregator;
using Newtonsoft.Json;

namespace SuperMarktProdParser
{
    public class SuperMarktParser: IModule,IParserModule
    {
        public string Name { get { return "Продовольственные супермаркеты в Москве"; } }

        public string SourceName { get { return "SupMark.json"; } }
        public DataModel[] Parse(string data)
        {
            string city, street;
            SuperMarkt[] supmar = JsonConvert.DeserializeObject<SuperMarkt[]>(data);
            List<DataModel> models = new List<DataModel>();
            foreach (SuperMarkt item in supmar)
            {
                string[] parts = item.Address.Split(',');
                city = "Москва";
                street = "";
                int addrIndex = 0;
                if (parts.Length > 0)
                    if (parts[0].ToLower().Contains("город "))
                    {
                        city = parts[0].Replace("город", "").Trim();
                        addrIndex++;
                    }
                if (parts.Length > addrIndex)
                    street = parts[addrIndex].Trim();
                DataModel model = models.FirstOrDefault(m => m.City == city && m.Street == street);
                if (model == null)
                {
                    model = new DataModel() { City = city, Street = street };
                    models.Add(model);
                }
                ObjectModel obj = new ObjectModel();
                obj.Kind = item.TypeObject;
                obj.Address = string.Join(", ", parts.Skip(addrIndex + 1)).Trim();
                obj.Name = item.Name;
                if (item.PublicPhone.Length > 0)
                    obj.Properties.Add(new DataProperty("Телефон", string.Join(", ", item.PublicPhone.Select(pp => pp.PublicPhoneR))));
                model.Objects.Add(obj);
            }
            return models.ToArray();
        }
    }
}
