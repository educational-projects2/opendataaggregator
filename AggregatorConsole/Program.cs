﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using aggregator;

namespace AggregatorConsole
{
    class Program
    {
        static void Main( string[] args )
        {
            // Задаем выходной каталог
            string outputFolder = "C:\\Data";
            // Создаем Агрегатор
            Aggregator aggregator = new Aggregator( outputFolder );
            // Получаем список тестовых файлов
            string[] files = Directory.GetFiles( Path.Combine( Path.GetDirectoryName( System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName ), "TestData\\" ), "*.*" );
            // Обработка файлов
            foreach( string filename in files )
            {
                Console.WriteLine( string.Format( "Обработка файла: {0}", Path.GetFileName( filename ) ) );
                try
                {

                    aggregator.ProcessFile( filename );
                }
                catch ( Exception ex )
                {
                    Console.WriteLine( "Ошибка при обработке:" );
                    Console.WriteLine( ex.Message );
                }
                Console.WriteLine();
            }
            Console.WriteLine( "Обработка завершена." );
            Console.WriteLine( string.Format( "Выходные данные сохранены в {0}", outputFolder ) );
            Console.WriteLine( "Нажмите любую клавишу для завершения." );
            Console.ReadKey();
        }
    }
}
