﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using aggregator;

namespace BarPlacesParser
{
    public class BarPlacesParser : IModule, IParserModule
    {
        public string Name { get { return "Бары в Москве"; } }

        public string SourceName { get { return "data-20150625T0100 (1).json"; } }
        public DataModel[] Parse(string data)
        {
            string city, street;
            Bars[] bars = JsonConvert.DeserializeObject<Bars[]>(data);
            List<DataModel> models = new List<DataModel>();
            foreach (Bars item in bars)
            {
                // Выделяем из адреса город и улицу
                string[] parts = item.Address.Split(',');
                city = "Москва";
                street = "";
                int addrIndex = 0;
                if ( parts.Length > 0 )
                    if ( parts[0].ToLower().Contains( "город " ) )
                    {
                        city = parts[0].Replace( "город", "" ).Trim();
                        addrIndex++;
                    }
                if ( parts.Length > addrIndex )
                    street = parts[addrIndex].Trim();
                DataModel model = models.FirstOrDefault( m => m.City == city && m.Street == street );
                if ( model == null )
                {
                    model = new DataModel() { City = city, Street = street };
                    models.Add( model );
                }
                ObjectModel obj = new ObjectModel();
                obj.Kind = item.TypeObject;
                obj.Address = string.Join(", ", parts.Skip(addrIndex + 1)).Trim();
                obj.Name = item.Name;
                if (item.PublicPhone.Length > 0)
                    obj.Properties.Add(new DataProperty("Телефон", string.Join(", ", item.PublicPhone.Select(pp => pp.PublicPhoneR))));
                model.Objects.Add( obj );
            }
            return models.ToArray();
        }
    }
}
