﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using aggregator;

namespace FileInput
{
    /// <summary>
    /// Модуль ввода даных из файла
    /// </summary>
    public class FileInput : IModule, IInputModule
    {
        public string Name {get {return "Ввод из файла";}}

        public DataInputSource InputSource { get { return DataInputSource.File; } }
        public string ReadData( string resourceName )
        {
            return File.ReadAllText( resourceName, Encoding.UTF8 );
        }
    }
}
