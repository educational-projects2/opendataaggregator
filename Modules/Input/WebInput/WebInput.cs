﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using aggregator;

namespace FileInput
{
    /// <summary>
    /// Модуль ввода даных из интернета
    /// </summary>
    public class WebInput : IModule, IInputModule
    {
        public string Name{ get {return "Загрузка с веб-сайта";}}

        public DataInputSource InputSource { get { return DataInputSource.Web; } }

        public string ReadData( string resourceName )
        {
            return null;
        }
    }
}
