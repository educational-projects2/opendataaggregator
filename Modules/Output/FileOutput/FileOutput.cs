﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using aggregator;
using System.IO;

namespace FileOutput
{
    /// <summary>
    /// Модуль вывода даных в файл
    /// </summary>
    public class FileOutput : IModule, IOutputModule
    {
        public string Name{ get {return "Вывод в файл"; } }

        public string OutputFolder { get; set; }

        public DataOutputWay OutputWay { get { return DataOutputWay.File; } }

        public void WriteData( DataModel[] models )
        {
            if ( string.IsNullOrEmpty( OutputFolder ) )
                throw new Exception( "Выходной каталог не задан!" );
            foreach ( DataModel dataModel in models )
            {
                if ( string.IsNullOrEmpty( dataModel.Country ) )
                    dataModel.Country = "Росссия";
                if ( string.IsNullOrEmpty( dataModel.City ) )
                    dataModel.City = "Неизвестно";
                string filename = Path.Combine( OutputFolder, string.Format( "{0}\\{1}\\{2}.json", dataModel.Country, dataModel.City, dataModel.Street ?? "Неизвестно" ) );
                if ( !Directory.Exists( Path.GetDirectoryName( filename ) ) )
                    Directory.CreateDirectory( Path.GetDirectoryName( filename ) );

                if ( File.Exists( filename ) )
                {
                    DataModel model = JsonConvert.DeserializeObject<DataModel>( File.ReadAllText( filename, Encoding.UTF8 ) );
                    model.Objects.AddRange( dataModel.Objects );
                    File.WriteAllText( filename, JsonConvert.SerializeObject( model, Formatting.Indented ), Encoding.UTF8 );
                }
                else
                    File.WriteAllText( filename, JsonConvert.SerializeObject( dataModel, Formatting.Indented ), Encoding.UTF8 );
            }
        }
    }
}
