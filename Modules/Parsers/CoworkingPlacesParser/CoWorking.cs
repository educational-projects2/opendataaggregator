﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoworkingPlacesParser
{
    /// <summary>
    /// 
    /// </summary>
    public class CoWorking
    {
        public string ID { get; set; }
        //public string FullName { get; set; }
        public string ShortName { get; set; }
        //public string ShortDescription { get; set; }
        //public string AdmArea { get; set; }
        //public string District { get; set; }
        public string Address { get; set; }
        //public string LocationClarification { get; set; }
        //public string Tariffs{ get; set; }
        //public string WorkPlaceTime{ get; set; }
        //public string YearOfCreation{ get; set; }
        public string Specialization{ get; set; }
        public ContactPhone[] ContactPhone{ get; set; }
        public Email[] Email{ get; set; }
        //public string Site_en{ get; set; }

    }
}
