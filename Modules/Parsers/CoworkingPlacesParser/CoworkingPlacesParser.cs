﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using aggregator;

namespace CoworkingPlacesParser
{
    public class CoworkingPlacesParser : IModule, IParserModule
    {
        public string Name {get {return "Места для коворкинга в Москве";} }

        public string SourceName { get { return "data-20200109T0100.json"; } }

        public DataModel[] Parse( string data )
        {
            string city, street;
            CoWorking[] coWorkings = JsonConvert.DeserializeObject<CoWorking[]>( data );
            List<DataModel> models = new List<DataModel>();
            foreach ( CoWorking item in coWorkings )
            {
                // Выделяем из адреса город и улицу
                string[] parts = item.Address.Split( ',' );

                city = "Москва";
                street = "";
                int addrIndex = 1;

                if ( parts.Length > 0 )
                    city = parts[0].Replace( "город", "" ).Trim();

                if ( parts.Length > 1 && parts[1].Contains( "город" ) )
                {
                    city = parts[1].Replace( "город", "" ).Trim();
                    addrIndex++;
                }

                if ( parts.Length > addrIndex )
                    street = parts[addrIndex].Trim();

                DataModel model = models.FirstOrDefault( m => m.City == city && m.Street == street );
                if ( model == null )
                {
                    model = new DataModel() { City = city, Street = street };
                    models.Add( model );
                }
                ObjectModel obj = new ObjectModel();
                if (parts.Length > 0)
                    obj.Kind = item.Specialization;
                obj.Address = string.Join( ", ", parts.Skip( addrIndex + 1 ) ).Trim();
                obj.Name = item.ShortName;
                if ( item.ContactPhone.Length > 0 )
                    obj.Properties.Add( new DataProperty( "Телефон", string.Join( ", ", item.ContactPhone.Select( cp => cp.ContactPhoneR ) ) ) );
                if ( item.Email.Length > 0 )
                    obj.Properties.Add( new DataProperty( "Email", string.Join( ", ", item.Email.Select( cp => cp.EmailR ) ) ) );
                model.Objects.Add( obj );
            }
            return models.ToArray();
        }
    }
}


